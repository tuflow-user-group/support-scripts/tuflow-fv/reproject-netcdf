#Reprojects NetCDF.  In this case the projection isn't assigned within the NetCDF so this is assigned first before reprojecting to a custom CRS.
#Written by DRK, 15th July 2021.

import os
import xarray as xr  # Need to make sure rioxarray is also installed.
from tkinter import Tk
from tkinter.filedialog import askopenfilename

# Tkinter to Select Netcdf file
root = Tk()
File_Name = askopenfilename(filetypes=[("NetCDF", "*.nc")], title="Select NetCDF file to be processed")  # Select Netcdf
root.withdraw()
directory = os.path.splitext(File_Name)[0]  # splits file name

# Open Netcdf File
nc = xr.open_dataset(File_Name)  # Open netcdf
# print(nc) #

# Set CRS where one is not currently set in the netcdf
nc = nc.rio.write_crs(
    "epsg:4326")  # Write coordinate system to netcdf file if not present.  Change accordingly.  Can be EPSG Code or Custom Proj String.
# print(nc.rio.crs)

# Reproject the netcdf.
nc_reprojected = nc.rio.reproject(
    '+proj=tmerc +lat_0=0 +lon_0=36 +k=0.9996 +x_0=500000 +y_0=0 +datum=WGS84 +units=m +no_defs')  # Reproject netcdf file.  Change accordingly, can be EPSG Code or Custom Proj String.
# print(nc.rio.crs)

# Save netcdf file
nc_reprojected.to_netcdf(
    directory + '_reprojected.nc')  # Write out reprojected netcdf.  Change to desired location/filename
# print(nc_reprojected)
